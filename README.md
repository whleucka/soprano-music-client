# Soprano Client

### Install: 
```
npm install && npm run build
```

### Dev server:
```
npm start
```

### Example .env file:
```
REACT_APP_SERVER_DOMAIN="yourdomain.com"
REACT_APP_API_TOKEN="user_api_token"
REACT_APP_API_URL="${REACT_APP_SERVER_DOMAIN}/api"
REACT_APP_API_STREAM_URL="${REACT_APP_SERVER_DOMAIN}/stream/"
REACT_APP_AUTH_URL="${REACT_APP_API_URL}/auth"
REACT_APP_ARTISTS_URL="${REACT_APP_API_URL}/all-artists"
REACT_APP_ARTIST_URL="${REACT_APP_API_URL}/artist"
REACT_APP_ALBUMS_URL="${REACT_APP_API_URL}/all-albums"
REACT_APP_ALBUM_URL="${REACT_APP_API_URL}/album"
REACT_APP_TRACKS_URL="${REACT_APP_API_URL}/all-tracks"
REACT_APP_LIKED_URL="${REACT_APP_API_URL}/liked"
REACT_APP_LIKE_URL="${REACT_APP_API_URL}/like"

```
