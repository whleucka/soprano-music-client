import React from 'react';

class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return <section id="settings" className="container">
            <div className="p-5">
                <h4>Settings</h4>
                <div className="mt-3">
                    <strong className="text-danger">Warning</strong>
                    <p>Not yet implemented</p>
                </div>
            </div>
        </section>
    }
}

export default Settings;