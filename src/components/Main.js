import React from 'react';
import Soprano  from "./Soprano";
import SignIn from "./SignIn";

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            auth: false,
            name: null,
            uuid: null,
            status: 'ready'
        }
    }

    componentDidMount() {
        const headers = {
            Authorization: `Bearer ${process.env.REACT_APP_API_TOKEN}`,
            Accept: 'application/json'
        };
        fetch(process.env.REACT_APP_AUTH_URL, {
            headers: headers
        })
            .then(res => res.json())
            .then((result) => {
                if (result.email_verified_at && result.email_verified_at !== '') {
                    this.setState({uuid: result.id, name: result.name, auth: true});
                }
            })
            .catch(err => {
                this.setState({status: 'error'}, () => console.log(err));
            });
    }

    render() {
        return <section id="main">
            {
                this.state.auth &&
                    <Soprano name={this.state.name} uuid={this.state.uuid} />
            }
            {
                !this.state.auth &&
                    <SignIn/>
            }
        </section>
    }
}

export default Main;