import React from 'react';
import TrackList from './TrackList'

class Playlist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return <section id="playlist" className="container">
            {
                this.props.tracks.map((track, key) => {
                    return <TrackList handleLike={this.props.handleLike} setTrackList={this.props.setTrackList}
                                                  index={key} key={key} track={track}/>
                })
            }
        </section>
    }
}

export default Playlist;