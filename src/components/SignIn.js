import React from 'react';
import {Button, Card} from 'react-bootstrap';

const SignIn = (props) => {
    return <div className="p-5 container">
        <Card>
            <Card.Header as="h5"><i className="fas fa-exclamation-triangle text-danger mr-2"/> Authentication Required</Card.Header>
            <Card.Body>
                <Card.Title>Your e-mail account must be verified to use this app</Card.Title>
                <Button className="mt-3" variant="success" size="sm" onClick={_ => {
                    window.location.href = process.env.REACT_APP_EMAIL_VERIFY;
                }}><i className="fas fa-headphones mr-2"/> Verify E-mail</Button>
            </Card.Body>
        </Card>
    </div>
};

export default SignIn;