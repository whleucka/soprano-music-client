import React from 'react';
import {ListGroup} from 'react-bootstrap';

const ArtistList = (props) => {
    const setArtist = (e) => {
        props.setArtist(e);
    };

    return <ListGroup.Item className="border-0">
        <div onClick={setArtist} id={props.artist} className="artist-list">
            <i className="far fa-user-circle mr-2"></i> {props.artist}
        </div>
    </ListGroup.Item>
};

export default ArtistList;