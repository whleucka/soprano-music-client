import React from 'react';
import {Card} from 'react-bootstrap';

const ArtistCard = (props) => {
    const setArtist = (e) => {
        props.setArtist(e);
    };

    return <Card onClick={setArtist} id={props.artist} className="artist-card">
        <Card.Body>
            <Card.Img variant="top"/>
            <Card.Title>
                <div className='artist-title'><strong>{props.artist}</strong></div>
            </Card.Title>
            <Card.Text>

            </Card.Text>
        </Card.Body>
    </Card>
};

export default ArtistCard;