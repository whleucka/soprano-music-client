import React from 'react';

const TrackList = (props) => {
    const title = props.track.artist + ' - ' + props.track.album + ' - ' + props.track.track;
    const liked_class = (props.track.liked) ? 'fas fa-heart pink mr-2' : 'far fa-heart pink mr-2';
    return <div className="track-list row" title={title}>
        <div className="col-12">
            <div>
                <span className="float-left" id={props.track.id} data-index={props.index} onClick={props.handleLike}>
                    <i id={'icon_' + props.track.id} className={liked_class}/>
                </span>
                <span className="float-left truncate" onClick={props.setTrackList}
                      id={props.index}>{props.track.artist} &#8212; {props.track.title}</span>
                <span className="float-right text-right">{props.track.playtime_string}</span>
            </div>
        </div>
    </div>
};

export default TrackList;