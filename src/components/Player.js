import React from 'react';
import {Button, ProgressBar} from 'react-bootstrap';
import Marquee from "react-smooth-marquee"

class Player extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        const play = "fa-play";
        const pause = "fa-pause";
        const prev = "fa-step-backward";
        const next = "fa-step-forward";
        let shuffle = "fa-random";
        if (this.props.shuffle)
            shuffle = shuffle + ' text-success';
        let playPause = play;
        if (Object.keys(this.props.track).length && this.props.status === 'playing' ) {
            playPause = pause;
        }

        const track_id = (Object.keys(this.props.track).length) ? this.props.track.id : null;
        const index = this.props.index;
        const coverArt = (Object.keys(this.props.track).length) ? process.env.REACT_APP_SERVER_DOMAIN + this.props.track.cover : "/img/no-album-art.png";
        const artist = (Object.keys(this.props.track).length) ? this.props.track.artist : 'No Artist';
        const title = (Object.keys(this.props.track).length) ? this.props.track.title : 'No Track';
        const playtime = (Object.keys(this.props.track).length) ? this.props.track.playtime_string : '0:00';
        const progress_color = (this.props.status === 'paused') ? 'secondary' : 'success';
        const liked_class = (this.props.track.liked) ? 'fas fa-heart pink mr-2' : 'far fa-heart pink mr-2';

        return <div className="fixed-bottom bg-light border-top">
            <div id="player" className="container">
                <ProgressBar id="player-progress" min={0} max={100} variant={progress_color} animated
                             now={this.props.progress}/>
                <div className="media">
                    <img id="player-cover" className="mr-2" src={coverArt} alt="cover"/>
                    <div className="media-body">
                        <div id="player-title">
                            {
                                (this.props.status === 'paused' || playtime === '0:00') && this.props.status !== 'loading' &&
                                <div className="truncate">
                                    <strong>{artist}</strong> &#8212; {title}
                                </div>
                            }
                            {
                                this.props.status === 'loading' &&
                                <div className="truncate">
                                    <strong>Loading...</strong>
                                </div>
                            }
                            {
                                this.props.status === 'playing' && playtime !== '0:00' && this.props.status !== 'loading' &&
                                <div className="truncate marquee-cont">
                                    <Marquee velocity={0.02}><strong>{artist}</strong> &#8212; {title}</Marquee>
                                </div>
                            }
                        </div>
                        <div id="player-controls">
                            <Button onClick={this.props.prevTrack} variant="light" className="player-control mr-1">
                                <i className={'fas ' + prev}></i>
                            </Button>
                            <Button onClick={this.props.playPause} variant="light" id="play-pause-button"
                                    className="player-control mr-1">
                                <i className={'fas ' + playPause}></i>
                            </Button>
                            <Button onClick={this.props.nextTrack} variant="light" className="player-control mr-1">
                                <i className={'fas ' + next}></i>
                            </Button>
                            <Button onClick={this.props.toggleShuffle} variant="light" className="player-control mr-1">
                                <i className={'fas ' + shuffle}></i>
                            </Button>
                            {/*<span className="player-like" id={track_id} data-index={index} onClick={this.props.handleLike}>
                                <i id={'player_icon_' + track_id} className={liked_class}/>
                            </span>*/}
                            <div id="playtime"><span id="playtime-string">0:00</span> / {playtime}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default Player;