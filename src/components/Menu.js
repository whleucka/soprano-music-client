import React from 'react';
import {Button, FormControl, InputGroup, Nav, Navbar, NavDropdown} from 'react-bootstrap';
import {LinkContainer} from "react-router-bootstrap";
import {withRouter} from "react-router-dom";

const Menu = (props) => {
    const setSearch = () => {
        props.setSearchTerm();
        props.history.push("/playlist");
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            setSearch();
        }
    };

    const handleOnClick = (e) => {
        setSearch();
    };

    const resetSearch = (e) => {
        document.getElementById('search-term').value = "";
        setSearch();
    }

    return <section id="menu">
        <Navbar collapseOnSelect className="border-bottom" variant="light" bg="light" fixed="top">
            <div className="container">
                <LinkContainer to="/">
                    <Navbar.Brand>
                        <i className="fas fa-music mr-1"/> Soprano
                    </Navbar.Brand>
                </LinkContainer>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="ml-auto">
                        <InputGroup>
                            <FormControl
                                id="search-term"
                                placeholder="Artist, Album, or Title"
                                aria-label="Global search"
                                aria-describedby="soprano-search"
                                onKeyDown={handleKeyDown}
                            />
                            <InputGroup.Append>
                                <Button onClick={handleOnClick} variant="outline-success">
                                    <i className="fas fa-search"></i>
                                </Button>
                                 <Button onClick={resetSearch} variant="outline-success">
                                     <i className="fas fa-undo"></i>
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Nav>
                </Navbar.Collapse>
            </div>
        </Navbar>
    </section>
};

export default withRouter(Menu);