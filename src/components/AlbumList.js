import React from 'react';
import {Image, ListGroup} from 'react-bootstrap';

const AlbumList = (props) => {
    const setAlbum = (e) => {
        props.setAlbum(e);
    };

    return <ListGroup.Item className="border-0">
        <div onClick={setAlbum} id={props.album} className="album-list">
            <Image className='rounded album-art-sm' src={props.cover}/>
            {props.album}
        </div>
    </ListGroup.Item>
};

export default AlbumList;