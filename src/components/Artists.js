import React from 'react';
import {ListGroup, Nav} from "react-bootstrap";
import ArtistCard from "./ArtistCard";
import ArtistList from "./ArtistList";
import {withRouter} from "react-router-dom";

class Artists extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeKey: 'list'
        }
    }

    componentDidMount() {
        if (document.getElementById('link-artists'))
            document.getElementById('link-artists').classList.add('active');
        if (document.getElementById('link-playlist'))
            document.getElementById('link-playlist').classList.remove('active');
    }

    viewSelectHandler = (key) => {
        this.setState({activeKey: key});
    };

    setArtistRedirect = (e) => {
        this.props.setArtist(e);
        this.props.history.push("/playlist");
        if (document.getElementById('link-artists'))
            document.getElementById('link-artists').classList.remove('active');
        if (document.getElementById('link-playlist'))
            document.getElementById('link-playlist').classList.add('active');
    };

    render() {
        return <section id="artists" className="container">
            <Nav className="py-3" variant="pills" defaultActiveKey="list" onSelect={this.viewSelectHandler}>
                <Nav.Item>
                    <Nav.Link eventKey="list">
                        List
                    </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="card">
                        Card
                    </Nav.Link>
                </Nav.Item>
            </Nav>
            {
                this.props.artists.length && this.state.activeKey === 'card' &&
                <div className="d-flex flex-wrap justify-content-around pb-5 mb-5">
                    {this.props.artists.map((data, index) => {
                        return <ArtistCard setArtist={this.setArtistRedirect} key={index} artist={data.artist}/>
                    })}
                </div>
            }
            {
                this.props.artists.length && this.state.activeKey === 'list' &&
                <ListGroup>
                    {this.props.artists.map((data, index) => {
                        return <ArtistList setArtist={this.setArtistRedirect} key={index} artist={data.artist}/>
                    })}
                </ListGroup>
            }
        </section>
    }
}

export default withRouter(Artists);