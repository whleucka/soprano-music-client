import React from 'react';
import Index from './Index';
import Artists from './Artists';
import Albums from './Albums';
import NowPlaying from './NowPlaying';
import Playlist from './Playlist';
import Playlists from './Playlists';
import Player from './Player';
import Menu from './Menu';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

class Soprano extends React.Component {
    playbackTimer = null;
    progressTimer = null;

    constructor(props) {
        super(props);
        this.state = {
            init: false,
            status: 'idle',
            searchTerm: '',
            currentTrack: {},
            currentIndex: null,
            audioUrl: null,
            artists: [],
            albums: [],
            tracks: [],
            playHistory: [],
            shuffle: true,
            progress: 0
        }
    }

    componentDidMount = () => {
        this.setState({
            status: 'ready',
        }, _ => {
            this.getTracks(process.env.REACT_APP_TRACKS_URL);
            //this.getLiked();
            this.getArtists(process.env.REACT_APP_ARTISTS_URL);
            this.getAlbums(process.env.REACT_APP_ALBUMS_URL);
        });
    };

    componentWillUnmount() {
        this.clearTimers();
    }

    getArtists = (api_url) => {
        const status = this.state.status;
        this.setState({status: 'loading'}, _ => {
            const headers = {
                Authorization: `Bearer ${process.env.REACT_APP_API_TOKEN}`,
                Accept: 'application/json'
            };
            fetch(api_url, {
                headers
            })
                .then(res => res.json())
                .then((result) => {
                    if (result)
                        this.setState({artists: result, status: status})
                })
                .catch(err => {
                    this.setState({status: 'error'}, () => console.log(err));
                });
        });
    };

    getAlbums = (api_url) => {
        const status = this.state.status;
        this.setState({status: 'loading'}, _ => {
            const headers = {
                Authorization: `Bearer ${process.env.REACT_APP_API_TOKEN}`,
                Accept: 'application/json'
            };
            fetch(api_url, {
                headers
            })
                .then(res => res.json())
                .then((result) => {
                    if (result)
                        this.setState({albums: result, status: status})
                })
                .catch(err => {
                    this.setState({status: 'error'}, () => console.log(err));
                });
        });
    };

    getLiked = () => {
        const status = this.state.status;
        this.setState({status: 'loading'}, _ => {
            const headers = {
                Authorization: `Bearer ${process.env.REACT_APP_API_TOKEN}`,
                Accept: 'application/json'
            };
            fetch(process.env.REACT_APP_LIKED_URL, {
                headers
            })
                .then(res => res.json())
                .then((result) => {
                    if (result)
                        this.setState({tracks: result, status: status})
                })
                .catch(err => {
                    this.setState({status: 'error'}, () => console.log(err));
                });
        });
    };


    getTracks = (api_url) => {
        const status = this.state.status;
        this.setState({status: 'loading'}, _ => {
            const headers = {
                Authorization: `Bearer ${process.env.REACT_APP_API_TOKEN}`,
                Accept: 'application/json'
            };
            const params = {
                search: this.state.searchTerm,
                uuid: this.props.uuid
            };
            const esc = encodeURIComponent;
            const query = Object.keys(params)
                .map(k => `${esc(k)}=${esc(params[k])}`)
                .join('&');
            const url = api_url + `?${query}`;
            fetch(url, {
                headers
            })
                .then(res => res.json())
                .then((result) => {
                    if (result) {
                        this.setState({
                            init: true,
                            status: status,
                            tracks: this.state.tracks.concat(result),
                        })
                    }
                })
                .catch(err => {
                    this.setState({status: 'error'}, () => console.log(err));
                });
        });
    };

    toggleLikedIcon = (status, id) => {
        const liked = 'fas';
        const notliked = 'far';
        const icon = document.getElementById(id);
        if (icon) {
            if (status === 'on') {
                icon.classList.remove(notliked);
                icon.classList.add(liked);
            } else {
                icon.classList.remove(liked);
                icon.classList.add(notliked);
            }
        }
    };

    handleLike = (e) => {
        const track = e.currentTarget.id;
        const index = e.currentTarget.dataset.index;
        if (!track)
            return;
        const headers = {
            Authorization: `Bearer ${process.env.REACT_APP_API_TOKEN}`,
            Accept: 'application/json'
        };
        const params = {
            music_file: track,
            uuid: this.props.uuid
        };
        const esc = encodeURIComponent;
        const query = Object.keys(params)
            .map(k => `${esc(k)}=${esc(params[k])}`)
            .join('&');
        const url = `${process.env.REACT_APP_LIKE_URL}?${query}`;
        fetch(url, {
            headers
        })
            .then(res => res.json())
            .then((result) => {
                if (result) {
                    this.toggleLikedIcon(result.status, 'icon_' + track);
                    this.toggleLikedIcon(result.status, 'player_icon_' + track);
                }
            })
            .catch(err => {
                this.setState({status: 'error'}, () => console.log(err));
            });
    }

    clearTimers = () => {
        if (this.playbackTimer) {
            clearInterval(this.playbackTimer);
            clearInterval(this.progressTimer);
            this.resetProgress();
        }
    }

    resetProgress = () => {
        const progress = document.getElementsByClassName('progress-bar')[0];
        progress.style.width = '0%';
    }

    setAudioSrc = () => {
        this.clearTimers();
        this.setState({
            status: 'playing',
            audioUrl: `${process.env.REACT_APP_API_STREAM_URL}${this.state.currentTrack.id}`
        }, this.setTimer);
    };

    /**
     * Click handler
     * Set a track from TrackList (via Playlist)
     * @param e
     */
    setTrackList = (e) => {
        const index = e.currentTarget.id;
        this.setTrack(index);
    };

    setTrack = (index) => {
        if (index in this.state.tracks && parseInt(index) >= 0) {
            if (this.state.tracks[index] === this.state.currentTrack) {
                this.playPause();
                return;
            }
            this.setState({
                currentTrack: this.state.tracks[index],
                status: 'loading',
                currentIndex: parseInt(index)
            }, this.setAudioSrc);
        } else {
            console.log('Track index not found');
        }
    };

    setSearchTerm = () => {
        const term = document.getElementById('search-term').value.trim();
        if (term !== '')
            this.setState({searchTerm: term, currentIndex: null, tracks: []}, _ => {
                this.getTracks(process.env.REACT_APP_TRACKS_URL);
            });
        else
            this.setState({searchTerm: '', currentIndex: null, tracks: []}, _ => {
                this.getTracks(process.env.REACT_APP_TRACKS_URL)
            });
    };

    setAlbum = (e) => {
        const album = e.currentTarget.id;
        if (album !== '') {
            this.setState({searchTerm: album, currentIndex: null, tracks: []}, _ => {
                this.getTracks(process.env.REACT_APP_ALBUM_URL);
            });
        }
    };

    setArtist = (e) => {
        const artist = e.currentTarget.id;
        if (artist !== '') {
            this.setState({searchTerm: artist, currentIndex: null, tracks: []}, _ => {
                this.getTracks(process.env.REACT_APP_ARTIST_URL);
            });
        }
    };

    toggleShuffle = () => {
        this.setState({shuffle: !this.state.shuffle});
    };

    play = () => {
        const audio = document.getElementById('audio-player');
        audio.play();
    };

    pause = () => {
        const audio = document.getElementById('audio-player');
        audio.pause();
    };

    playPause = () => {
        const audio = document.getElementById('audio-player');
        /**
         * Play is pressed immediately
         */
        if (this.state.audioUrl === null && this.state.tracks.length >= 1) {
             this.setTrack(0);
        }

        /**
         * Play/pause track
         */
        if (!audio.paused)
            audio.pause();
        else
            audio.play()
                .then(_ => {
                    // Do something here with mediaSession?
                })
                .catch(err => console.log(err));

    };

    nextTrack = () => {
        if (this.state.currentIndex === null) {
            if (this.state.shuffle)
                this.shuffleTrack()
            else
                this.setTrack(0);
            return;
        }
        if (!this.state.shuffle) {
            const max = this.state.tracks.length;
            const index = this.state.currentIndex + 1;
            if (index < max)
                this.setTrack(index);
        } else {
            if (this.state.tracks.length > 1)
                this.shuffleTrack();
        }
    };

    prevTrack = () => {
        const index = this.state.currentIndex - 1;
        if (index > -1)
            this.setTrack(index);
    };

    shuffleTrack = () => {
        const max = this.state.tracks.length;
        const index = this.state.currentIndex;
        let rnd = Math.floor(Math.random() * max + 1);
        let result = false;
        while (!result) {
            if (rnd !== index) {
                result = true;
                this.setTrack(rnd);
            } else {
                rnd = Math.floor(Math.random() * max + 1);
            }
        }
    };

    setStatus = () => {
        const audio = document.getElementById('audio-player');
        let status = 'playing';
        if (!audio)
            return;
        if (audio.paused)
            status = 'paused';
        this.setState({status: status});
    };

    mediaSessionMeta = (title, artist, album, cover_src) => {
        if ('mediaSession' in navigator) {
            navigator.mediaSession.metadata = new window.MediaMetadata({
                title: title,
                artist: artist,
                album: album,
                artwork: [
                    {src: cover_src, sizes: '256x256', type: 'image/jpeg'}
                ]
            });
            navigator.mediaSession.setActionHandler('play', this.play);
            navigator.mediaSession.setActionHandler('pause', this.pause);
            /*navigator.mediaSession.setActionHandler('seekbackward', function() {});
            navigator.mediaSession.setActionHandler('seekforward', function() {});*/
            navigator.mediaSession.setActionHandler('previoustrack', this.prevTrack);
            navigator.mediaSession.setActionHandler('nexttrack', this.nextTrack);
        }
    };

    setTimer = () => {
        const audio = document.getElementById('audio-player');
        const playtime = document.getElementById('playtime-string');
        const progress = document.getElementsByClassName('progress-bar')[0];
        let seconds = 0;
        const max_seconds = this.state.currentTrack.playtime_seconds;
        const pad = function (num, size) {
            return ('000' + num).slice(size * -1);
        };
        if (playtime && max_seconds) {
            playtime.innerText = '0:00';
            this.playbackTimer = setInterval(function () {
                if (!audio.paused) {
                    if (seconds < max_seconds) {
                        seconds++;
                    } else {
                        return;
                    }
                    let time = parseFloat(seconds).toFixed(3);
                    let _hours = Math.floor(time / 60 / 60);
                    let _minutes = Math.floor(time / 60) % 60;
                    let _seconds = Math.floor(time - _minutes * 60);
                    let output = '';
                    if (_hours)
                        output += pad(_hours, 1) + ':';
                    output += pad(_minutes, 1) + ':' + pad(_seconds, 2);
                    playtime.innerText = output;
                }
            }, 1000);
            this.progressTimer = setInterval(function () {
                if (!audio.paused) {
                    const currTime = parseFloat(audio.currentTime);
                    const end = parseFloat(max_seconds);
                    const pct = ((currTime / end) * 100).toFixed(2);
                    progress.style.width = pct + '%';
                }
            }, 250);
        }
        // Set media session metadata
        this.mediaSessionMeta(this.state.currentTrack.title, this.state.currentTrack.artist,
            this.state.currentTrack.album, process.env.REACT_APP_SERVER_DOMAIN + this.state.currentTrack.cover)
    };

    render() {
        return <Router>
            <Menu setSearchTerm={this.setSearchTerm}/>
            <Switch>
                <Route path="/playlists">
                    <Playlists/>
                </Route>
                <Route path="/artists">
                    {
                        this.state.status !== 'error' &&
                        <Artists setArtist={this.setArtist} artists={this.state.artists}/>
                    }
                    {
                        this.state.status === 'error' &&
                        <div className="w-100 pt-5 text-center">
                            <i className="fas fa-bomb text-danger mr-2"/> <strong>Error</strong>
                        </div>
                    }
                </Route>
                <Route path="/albums">
                    {
                        this.state.status !== 'error' &&
                        <Albums setAlbum={this.setAlbum} albums={this.state.albums}/>
                    }
                    {
                        this.state.status === 'error' &&
                        <div className="w-100 pt-5 text-center">
                            <i className="fas fa-bomb text-danger mr-2"/> <strong>Error</strong>
                        </div>
                    }
                </Route>
                <Route path="/playlist">
                    {
                        this.state.status !== 'error' && this.state.tracks.length > 0 &&
                        <Playlist handleLike={this.handleLike} setTrackList={this.setTrackList}
                                  tracks={this.state.tracks}/>
                    }
                    {
                        (this.state.status === 'loading' || this.state.status === 'idle') &&
                        <div className="w-100 pt-5 text-center">
                            <i className="far fa-star text-warning fa-spin mr-2"/> <strong>Loading</strong>...
                        </div>
                    }
                    {
                        this.state.init === true && this.state.tracks.length === 0 && this.state.status !== 'loading' &&
                        <div className="w-100 pt-5 text-center">
                            <strong>No songs yet</strong>
                        </div>
                    }
                    {
                        this.state.status === 'error' &&
                        <div className="w-100 pt-5 text-center">
                            <i className="fas fa-bomb text-danger mr-2"/> <strong>Error</strong>
                        </div>
                    }
                </Route>
                <Route path="/liked">
                    {
                        this.state.status !== 'error' && this.state.tracks.length > 0 &&
                        <Playlist handleLike={this.handleLike} setTrackList={this.setTrackList}
                                  tracks={this.state.tracks}/>
                    }
                    {
                        (this.state.status === 'loading' || this.state.status === 'idle') &&
                        <div className="w-100 pt-5 text-center">
                            <i className="far fa-star text-warning fa-spin mr-2"/> <strong>Loading</strong>...
                        </div>
                    }
                    {
                        this.state.init === true && this.state.tracks.length === 0 && this.state.status !== 'loading' &&
                            <div className="w-100 pt-5 text-center">
                                <strong>No liked songs yet</strong>
                            </div>
                    }
                    {
                        this.state.status === 'error' &&
                        <div className="w-100 pt-5 text-center">
                            <i className="fas fa-bomb text-danger mr-2"/> <strong>Error</strong>
                        </div>
                    }
                </Route>
                <Route path="/now-playing">
                    <NowPlaying/>
                </Route>
                <Route exact path="/">
                    <Index getLiked={_ => {
                        this.setState({tracks: []}, _ => this.getLiked());
                    }
                    } getTracks={_ => {
                        document.getElementById('search-term').value = '';
                        this.setState({tracks: [], searchTerm: ''}, _ => this.getTracks(process.env.REACT_APP_TRACKS_URL));
                    }
                    } albums={this.state.albums.map(track => {
                        return track.cover;
                    })}/>
                </Route>
            </Switch>
            <Player index={this.props.currentIndex} handleLike={this.handleLike} playPause={this.playPause} play={this.play} pause={this.pause} nextTrack={this.nextTrack}
                    prevTrack={this.prevTrack} toggleShuffle={this.toggleShuffle} shuffle={this.state.shuffle}
                    status={this.state.status} track={this.state.currentTrack} progress={this.state.progress}/>
            <audio id="audio-player" src={this.state.audioUrl} onPause={this.setStatus} onPlaying={this.setStatus}
                   onEnded={_ => {
                       this.resetProgress();
                       this.nextTrack();
                   }} onLoadedData={this.playPause}/>
        </Router>
    }
}

export default Soprano;
