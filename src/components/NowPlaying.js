import React from 'react';

class NowPlaying extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return <section id="now-playing" className="container p-5">
            <h4 className="pb-3">Now Playing</h4>
            <div>
                <strong className="text-danger">Warning</strong>
                <p>Not yet implemented</p>
            </div>
        </section>
    }
}

export default NowPlaying;