import React from 'react';
import {Button, Card, Jumbotron} from 'react-bootstrap';
import {LinkContainer} from "react-router-bootstrap";

const timer = null;
const interval = 3000;

class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cover: '',
            index: 0
        }
    }

    setRandomCover = () => {
        const max = this.props.albums.length;
        let rnd = Math.floor(Math.random() * max + 1);
        let result = false;
        const cover = document.getElementById("rnd-cover");
        while (!result) {
            if (this.state.index !== rnd) {
                result = true;
            } else {
                rnd = Math.floor(Math.random() * max + 1);
            }
        }
        if (this.mounted && cover)
            this.setState({index: rnd}, _ => {
                if (typeof (this.props.albums[rnd]) !== 'undefined')
                    cover.src = process.env.REACT_APP_SERVER_DOMAIN + this.props.albums[rnd];
            });
    }

    componentDidMount() {
        this.mounted = true;
        if (!timer)
            setInterval(this.setRandomCover, interval);
        if (document.getElementById('link-playlist'))
            document.getElementById('link-playlist').classList.remove('active');
        if (document.getElementById('link-albums'))
            document.getElementById('link-albums').classList.remove('active');
        if (document.getElementById('link-artists'))
            document.getElementById('link-artists').classList.remove('active');
    }

    componentWillUnmount() {
        this.mounted = false;
        clearInterval(timer);
    }

    render() {
        return <section id="index" className="container pt-3 pb-5">
            <Jumbotron>
                <div id="jumbo-content" className="mx-auto">
                    <h1 className="main-title display-1"><i className="fas fa-music mr-1"/> Soprano</h1>
                    <p className="lead p-2 mb-2">
                        Music streaming, simplified.
                    </p>
                    <div className="supported-devices mt-2">
                        <span><i className="fab fa-android"/> Android</span> | <span><i className="fab fa-apple"/> Apple</span> | <span><i
                        className="fab fa-windows"/> Microsoft</span> | <span><i className="fab fa-linux"/> Linux</span>
                    </div>
                </div>
            </Jumbotron>
            <Card className="mt-0">
                <Card.Body>
                    <div>
                        <img src="/img/no-album-art.png" onError={_ => this.src = "/img/no-album-art.png"} alt="cover"
                             id="rnd-cover" className="float-left mr-3"/>
                    </div>
                    <LinkContainer to="albums">
                        <Button variant="outline-secondary" size="sm"  className="mt-2">
                            Browse Albums
                        </Button>
                    </LinkContainer>
                </Card.Body>
            </Card>
            <Card className="mt-1">
                <Card.Body>
                    <div>
                        <img src="/img/alexisonfire.jpg" alt="cover" id="artist-cover" className="float-left mr-3"/>
                    </div>
                    <LinkContainer to="artists">
                        <Button variant="outline-secondary" size="sm" className="mt-2">
                            Browse Artists
                        </Button>
                    </LinkContainer>
                </Card.Body>
            </Card>
            <Card className="mt-1">
                <Card.Body>
                    <div>
                        <img src="/img/atmosphere.jpg" alt="cover" id="track-cover" className="float-left mr-3"/>
                    </div>
                    <LinkContainer to="liked">
                        <Button onClick={this.props.getLiked} variant="outline-secondary" size="sm" className="mt-2">
                            Browse Liked
                        </Button>
                    </LinkContainer>
                </Card.Body>
            </Card>
            <Card className="mt-1 mb-5">
                <Card.Body>
                    <div>
                        <img src="/img/coheed.jpeg" alt="cover" id="track-cover" className="float-left mr-3"/>
                    </div>
                    <LinkContainer to="playlist">
                        <Button onClick={this.props.getTracks} variant="outline-secondary" size="sm" className="mt-2">
                            Browse Tracks
                        </Button>
                    </LinkContainer>
                </Card.Body>
            </Card>
        </section>
    }
}

export default Index;