import React from 'react';
import {Card} from 'react-bootstrap';

const AlbumCard = (props) => {
    const setAlbum = (e) => {
        props.setAlbum(e);
    };

    return <Card onClick={setAlbum} id={props.album} className="rounded-lg border-0 m-1 album-card">
        <Card.Body>
            <Card.Img className='rounded album-art' variant="top" src={props.cover}/>
            <Card.Title>
                <div className='album-title pt-2'><strong>{props.album}</strong></div>
            </Card.Title>
        </Card.Body>
    </Card>
};

export default AlbumCard;