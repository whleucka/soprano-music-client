import React from 'react';
import AlbumCard from "./AlbumCard";
import AlbumList from "./AlbumList";
import {ListGroup, Nav} from 'react-bootstrap';
import {withRouter} from "react-router-dom";

class Albums extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeKey: 'list'
        }
    }

    componentDidMount() {
        if (document.getElementById('link-albums'))
            document.getElementById('link-albums').classList.add('active');
        if (document.getElementById('link-playlist'))
            document.getElementById('link-playlist').classList.remove('active');
    }

    viewSelectHandler = (key) => {
        this.setState({activeKey: key});
    };

    setAlbumRedirect = (e) => {
        this.props.setAlbum(e);
        this.props.history.push("/playlist");
        if (document.getElementById('link-albums'))
            document.getElementById('link-albums').classList.remove('active');
        if (document.getElementById('link-playlist'))
            document.getElementById('link-playlist').classList.add('active');
    };

    render() {
        return <section id="albums" className="container">
            <Nav className="py-3" variant="pills" defaultActiveKey="list" onSelect={this.viewSelectHandler}>
                <Nav.Item>
                    <Nav.Link eventKey="list">
                        List
                    </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="card">
                        Card
                    </Nav.Link>
                </Nav.Item>
            </Nav>
            {
                this.state.activeKey === 'card' &&
                <div className="d-flex flex-wrap justify-content-around pb-5 mb-5">
                    {this.props.albums.map((data, index) => {
                        const cover_src = process.env.REACT_APP_SERVER_DOMAIN + '/' + data.cover;
                        return <AlbumCard setAlbum={this.setAlbumRedirect} key={index} album={data.album}
                                          cover={cover_src}/>
                    })}
                </div>
            }
            {
                this.state.activeKey === 'list' &&
                <ListGroup>
                    {this.props.albums.map((data, index) => {
                        const cover_src = process.env.REACT_APP_SERVER_DOMAIN + '/' + data.cover;
                        return <AlbumList setAlbum={this.setAlbumRedirect} key={index} album={data.album}
                                          cover={cover_src}/>
                    })}
                </ListGroup>
            }
        </section>
    }
}

export default withRouter(Albums);